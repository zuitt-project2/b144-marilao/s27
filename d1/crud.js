const http = require('http');

// Mock database
let directory = [
{
	"name": "Brandon",
	"email": "brandon@mail.com"
},
{
	"name": "Jobert",
	"email": "jobert@mail.com"
}
]


http.createServer((request,response) =>{
	// Route for returning all items upon receiving a GET request
	if(request.url == '/users' && request.method == 'GET'){
		response.writeHead(200, {"Content-Type": 'application/json'});
		response.write(JSON.stringify(directory));
		/*
			When info is sent to the client, it is sent in the format of a stringified JSON.
			After the client received the stringified JSON then it is converted back into a JSON object to be consumed.
		*/
		response.end()
	}
	if(request.url == '/users' && request.method == 'POST'){
		// request empty body for a string
		// Declare and initialize requestBody variable to an empty string
		// Act as placeholder for the data to be created later on
		let requestBody = '';
		// Stream => it is a sequence of data
		//1. Data stream = data is received from the client ad processed in the 'data' stream called 'data' and the code below will be triggered
		// data step - this reads the data stream and process it as the request body
		request.on('data', function(data){
			// Assigns the data retrieved from the data stream to requestBody
			requestBody += data;
		})

		// 2. end step = only runs after the request has completely been sent(response once the data is already processed)
		request.on('end', function(){
			// check if at this point the requestBody is of data type STRING
			// we need this to be of data type JSOn to access its properties
			console.log(typeof requestBody)

			// converts string requestBody to JSON

			requestBody = JSON.parse(requestBody);

			// create new object representing the new mock database record
			let newUser = {
				'name': requestBody.name,
				'email': requestBody.email
			}
			// to add data or new user into the mock database
			directory.push(newUser)
			console.log(directory)

			response.writeHead(200, {'Content-Type': 'application/json'})
			response.write(JSON.stringify(newUser));
			response.end()
		})
	}

}).listen(4000)

console.log('Server running at localhost:4000')